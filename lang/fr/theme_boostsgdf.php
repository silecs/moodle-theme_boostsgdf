<?php
/**
 * @package    theme
 * @subpackage boostsgdf
 * @copyright  2020 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['choosereadme'] = 'Le thème sgdf dérive de Boost.';
$string['pluginname'] = 'Boostsgdf';
$string['configtitle'] = 'Boostsgdf';

$string['advancedsettings'] = 'Réglages avancés';
$string['backgroundimage'] = 'Image de fond';
$string['backgroundimage_desc'] = 'L\'image à afficher comme arrière-plan du site. L\'image de fond déposée ici remplacera l\'image de fond définie dans vos fichiers de préréglages de thème.';
$string['backtotop'] = 'Retour en haut de page et Scrollspy';
$string['brandcolor'] = 'Couleur principale';
$string['brandcolor_desc'] = 'La couleur majeure du thème.';
$string['currentinparentheses'] = '(actuel)';
$string['generalsettings'] = 'Réglages généraux';
$string['preset'] = 'Préréglage de thème';
$string['preset_desc'] = 'Veuillez choisir un préréglage pour modifier l\'aspect du thème.';
$string['presetfiles'] = 'Fichiers de réglages additionnels pour le thème';
$string['presetfiles_desc'] = 'Des fichiers de réglages peuvent être utilisés afin de changer totalement la présentation du thème. Voir <a href=https://docs.moodle.org/dev/Boost_Presets>Boost presets</a>  pour des informations sur la façon de créer et partager vos propres fichiers de réglages et consultez le <a href=http://moodle.net/boost>catalogue des fichiers de réglages</a> existants.';
$string['privacy:drawernavclosed'] = 'Le réglage actuel pour le tiroir de navigation est « fermé ».';
$string['privacy:drawernavopen'] = 'Le réglage actuel pour le tiroir de navigation est « ouvert ».';
$string['privacy:metadata'] = 'Le thème Iepel n\'enregistre aucune donnée personnelle.';
$string['privacy:metadata:preference:draweropennav'] = 'Le réglage utilisateur pour afficher ou cacher le tiroir de navigation.';
$string['rawscss'] = 'SCSS brut';
$string['rawscss_desc'] = 'Ce champ permet d\'indiquer du code SCSS ou CSS qui sera injecté à la fin de la feuille de styles.';
$string['rawscsspre'] = 'Code SCSS initial';
$string['rawscsspre_desc'] = 'Ce champ permet d\'indiquer du code SCSS d\'initialisation, qui sera injecté dans la feuille de styles avant toute autre définition. La plupart du temps, ce code sera utilisé pour définir des variables.';
$string['region-side-pre'] = 'Droite';
