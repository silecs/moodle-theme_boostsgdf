<?php
/**
 * @package    theme
 * @subpackage boostsgdf
 * @copyright  2020 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once('renderers/core_renderer.php');
