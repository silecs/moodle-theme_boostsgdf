<?php
/**
 * @package    theme
 * @subpackage boostsgdf
 * @copyright  2020 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2020042400;
$plugin->requires  = 2019111200;
$plugin->component = 'theme_boostsgdf';

$plugin->dependencies = [
    'theme_boost' => 2019111800
];
